<?php

/**
 * Created by tomas
 * at 17.03.2023
 */

declare(strict_types=1);

namespace JSONAPI\Client;

use Psr\Http\Client\ClientInterface;

class Configuration
{
    private string $baseURL;
    private ClientInterface $http;

    /**
     * @return ClientInterface
     */
    public function getHttp(): ClientInterface
    {
        return $this->http;
    }
}
