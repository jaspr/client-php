<?php

/**
 * Created by tomas
 * at 17.03.2023
 */

declare(strict_types=1);

namespace JSONAPI\Client;


use GuzzleHttp\Psr7\Request;
use Psr\Http\Client\ClientInterface;


interface ResourceDocument
{
    public function data(): object;

    public function self();
}

interface CollectionDocument
{
    public function data(): array;

    public function self(): CollectionDocument;

    public function first();

    public function prev();

    public function next();

    public function last();
}


interface ResourceMethods
{
    public function self(): ResourceDocument;
}

interface CollectionMethods
{
    public function id(string $id): ResourceMethods;

    public function create();

    public function self(): CollectionDocument;

    public function filter(): CollectionMethods;

    public function limit(): CollectionMethods;

    public function offset(): CollectionMethods;

    public function fields(): CollectionMethods;

    public function sort(): CollectionMethods;

    public function param(): CollectionMethods;

    public function getType(): string;

    public function getQuery(): string;

}

class SelectResource implements ResourceMethods
{

    private CollectionMethods $collection;
    private string $id;
    private ClientInterface $client;

    public function __construct(ClientInterface $client, CollectionMethods $collection, string $id)
    {
        $this->collection = $collection;
        $this->id = $id;
        $this->client = $client;
    }

    public function self(): ResourceDocument
    {
        $request = new Request();
        $this->client->sendRequest();
    }

}

class SelectCollection implements CollectionMethods
{
    private string $type;

    public function __construct(string $type)
    {
        $this->type = $type;
    }

    public function id(string $id): ResourceMethods
    {
        return new SelectResource($id);
    }

    public function create(): ResourceDocument
    {
        // TODO: Implement create() method.
    }

    public function self(): CollectionDocument
    {
        // TODO: Implement self() method.
    }

    public function filter(): CollectionMethods
    {
        // TODO: Implement filter() method.
    }

    public function limit(): CollectionMethods
    {
        // TODO: Implement limit() method.
    }

    public function offset(): CollectionMethods
    {
        // TODO: Implement offset() method.
    }

    public function fields(): CollectionMethods
    {
        // TODO: Implement fields() method.
    }

    public function sort(): CollectionMethods
    {
        // TODO: Implement sort() method.
    }

    public function param(): CollectionMethods
    {
        // TODO: Implement param() method.
    }
}

class Client
{
    private ClientInterface $http;

    private string $type;
    private string $id;


    public function __construct(Configuration $configuration)
    {
        $this->http = $configuration->getHttp();
    }

    public function select(string $type): CollectionMethods
    {
        return new SelectCollection($type);
    }
}

$c = new Client();
